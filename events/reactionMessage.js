const { Events } = require('discord.js');
const filter = ['clase','clases','entrega','tarea','tareas','entregas','evaluacion','evaluada','hoch','presentacion','presentación','nota','notas','revision','revisión','prueba','profe','profesor','hora','ayuda','fecha',
'pregunta','consulta','video','informe']


module.exports = {
	name: Events.MessageCreate,
	async execute(interaction) {
        let message = interaction.content.toLowerCase()
        const found = filter.some(palabra => message.includes(palabra));
        if (found) {
            interaction.react('🫂')
        }
	},
};