const { SlashCommandBuilder } = require('discord.js');
const axios = require('axios')

module.exports = {
	data: new SlashCommandBuilder()
		.setName('consulta')
		.setDescription('Realizar consulta BOT Ayudante')
        .addStringOption(option =>
            option.setName('consulta')
                .setDescription('The input to echo back')
                .setRequired(true)),
	async execute(interaction) {
        let texto = interaction.options.getString('consulta')
        let url
        texto = texto.replace(/ /g, '+');
        await axios.get(`https://gprivate.com/functions/shorten.php?https://letmegooglethat.com/?q=${texto}`).then(r=>{
            url = r.data.shorturl
        })
		await interaction.reply("Puedes acceder a la respuesta en el siguiente link: "+url);
	},
};